RED='\033[0;31m'
NC='\033[0m' # No Color

if [ ! -d roles/ssh/dotssh ]; then
  echo -e "${RED}"
  echo "You do not have a dotssh folder in the playbook directories."
  echo "I will create the template folder for you in roles/ssh/dotssh"
  echo "You should populate this folder with any files you want to put into"
  echo "~/.ssh (ssh keys, ssh config, etc.)"
  echo -e "${NC}"
  cp -r roles/ssh/RENAME_ME roles/ssh/dotssh
  echo -e "${RED}"
  echo "Script will terminate now so you can populate the files in that folder."
  echo "If you do not need/want to copy anything to ~/.ssh, then just run the script again"
  echo "(you won't see this message the second time)"
  echo -e "${NC}"
  echo ""
  exit 1
fi

echo "Running ansible. Press a key to start."
echo "When prompted for \"BECOME password\", use your sudo password, or leave blank if you have passwordless sudo"
echo ""
echo "Specify extra ansible args using the environment variable EXTRA_ARGS. e.g.:"
echo "EXTRA_ARGS=\"--extra-vars=zerotier_network=abcdef\" ./run-app-installs.sh"

ansible-playbook app-installs.yml $EXTRA_ARGS -i hosts.yaml -K -e "login_user=$USER" $@

if [ $? -eq 0 ]; then
  echo "If everything succeeded, you should now:"
  echo "* Enable AppIndicator support in Gnome Extensions"
  echo "* Install VirtualBox extension pack: https://www.virtualbox.org/wiki/Downloads"
  echo "* Set the icon on the WeChat application Lutris"
  echo "  (point the icon browser to ~/.local/share/icons/hicolor/256x256/apps/8A76_WeChat.0.png)"
  echo "* Reboot"
else
  echo -e "${RED}Ansible did not complete successfully, check output above${NC}"
fi
