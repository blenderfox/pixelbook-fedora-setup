set -euxo pipefail

sudo cp -v firmware/9d71-GOOGLE-EVEMAX-0-tplg.bin /lib/firmware/
sudo cp -v firmware/dsp_lib_dsm_core_spt_release.bin /lib/firmware/
sudo cp -v firmware/intel/dsp_fw_C75061F3-F2B2-4DCC-8F9F-82ABB4131E66.bin /lib/firmware/intel
sudo mkdir -p /opt/google/dsm/ || true
sudo cp -v opt/google/dsm/dsmparam.bin /opt/google/dsm/dsmparam.bin

sudo systemd-hwdb update

sudo pacman -Sy --noconfirm manjaro-pulse pulseaudio pulseaudio-alsa pulseaudio-bluetooth pulseaudio-equalizer pulseaudio-jack pulseaudio-lirc pulseaudio-rtp pulseaudio-zeroconf

echo Now try rebooting
