# Setup Fedora/ArchLinux/Manjaro on Pixelbook

A basic ansible playbook for configuring applications on Fedora/ArchLinux/Manjaro on a Pixelbook

## Requirements
* Supported linux distribution. Currently:
    * Fedora
    * ArchLinux/Manjaro (other arch-based distros MAY work, but YMMV)

*WARNING*: Before running this script, and for your own protection, make sure you have a backup of your files and/or a disk image of your Pixelbook via something like CloneZilla in case something goes wrong while running this setup.

The `run-app-installs.sh` script runs a playbook for installing the apps I personally use -- you don't need to run it unless you want to have the same applications I do. :-)

The `run-app-installs-office.sh` script runs the same playbook, but only installs a subset of the apps -- namely ones that are directly related to office work, and no non-office-related stuff, so no Steam, no Spotify, no Wine, etc.

If you wish to run a specific task only, you can do so using:

```
ansible-playbook \
    app-installs.yml \
    -i hosts.yaml -K -e "login_user=$USER"
    -e "{\"tasks_list\":[task-a.yml, task-b.yml]}" \
```

or 

```
ansible-playbook \
    app-installs-office.yml \
    -i hosts.yaml -K -e "login_user=$USER"
    -e "{\"tasks_list\":[task-a.yml, task-b.yml]}" \
```

Some tasks may depend on previous tasks and so may break unless you include the parent task (normally `support.yml`)