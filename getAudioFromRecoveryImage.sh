set -euxo pipefail

BIN_VERSION=$(curl -s https://chromiumdash.appspot.com/cros/download_serving_builds_csv?deviceCategory=Chrome%20OS | grep -E "^eve" | cut -d, -f 6)

RECOVERY_IMG=https://dl.google.com/dl/edgedl/chromeos/recovery/chromeos_${BIN_VERSION}_eve_recovery_stable-channel_mp-v3.bin.zip
RECOVERY_ZIP=$(basename $RECOVERY_IMG)
RECOVERY_BIN=$(basename $RECOVERY_IMG .zip)

echo Download $RECOVERY_IMG to $RECOVERY_ZIP
wget  --no-check-certificate -c $RECOVERY_IMG -O $RECOVERY_ZIP

#echo Remove $RECOVERY_BIN if it exists..
#rm -v $RECOVERY_BIN

echo Unzipping $RECOVERY_ZIP
unzip -n $RECOVERY_ZIP

which kpartx >/dev/null
if [ $? -ne 0 ]; then
  echo You do not have kpartx installed, it is needed
  echo For arch/manjaro, this is in multipath-tools package
  exit 1
fi

echo Create devicemaps
sudo kpartx -dv $RECOVERY_BIN
MOUNT_DEV=$(sudo kpartx -av $RECOVERY_BIN | head -n 3 | tail -n 1 | awk '{print $3}')

echo Using mount $MOUNT_DEV
sudo mount -o ro /dev/mapper/$MOUNT_DEV /mnt

mkdir -v firmware || true
mkdir -v opt || true
mkdir -v usr-share-alsa || true
mkdir -v usr-lib || true

echo Copying firmware files
cp -r /mnt/lib/firmware/* firmware/

echo Copy opt
cp -r /mnt/opt/* opt/

echo Copy /usr/share/alsa
cp -r /mnt/usr/share/alsa usr-share-alsa

echo Copy /usr/lib
cp -r /mnt/usr/lib/* usr-lib

echo Umounting and disconnecting
sudo umount /mnt

sudo kpartx -dv $RECOVERY_BIN

# echo Removing downloaded files 
# rm -v $RECOVERY_BIN $RECOVERY_ZIP