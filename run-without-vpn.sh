#!/bin/bash

# set -euxo pipefail

#~/makeAndRotateTimeShiftSnap.sh 

OS=$(cat /etc/os-release | grep -E "^ID=" | cut -d= -f2 | sed s/"\""/""/g)

case $OS in
  fedora)
    echo "Detected OS: $OS"
    #sudo dnf clean dbcache
    #sudo dnf makecache
    echo Check for rpm-ostree
    which rpm-ostree
    if [ $? -ne 0 ]; then
      echo No rpm-ostree, skipping using that
    else
      sudo rpm-ostree upgrade
    fi
    echo Check for dnf
    which dnf
    if [ $? -ne 0 ]; then
      echo "No dnf, can't continue!"
      exit 1
    else
      sudo dnf update --refresh
    fi
    ;;
  arch|manjaro|endeavouros)
    echo "Detected OS: $OS"
    sudo pacman -Syyu
    yay -Syyu

    # # Keep only last version of the package
    sudo pacman -Scc --noconfirm
    
    # ## Clear AUR cache in the same way
    yay -Scc --aur --noconfirm

    ;;
  *)
    echo "Unknown OS: $OS"
    exit 1
    ;;
esac

cd ~/work/repos/pixelbook-fedora-setup/files/

sudo snap refresh
sudo flatpak update

# for pkg in $(sudo pip3 list --outdated --format json | jq .[].name --raw-output); do echo "Upgrading $pkg";sudo pip3 install $pkg --upgrade;done
#for pkg in $(pip3 list --outdated --format json | jq .[].name --raw-output); do echo "Upgrading $pkg";pip3 install $pkg --upgrade;done
echo Pip review
pip-review -aC --user

ansible-galaxy collection install --upgrade community.docker community.general --force 

if [ $? -ne 0 ]; then
  ## hack
  echo Install failed, going to install resolvelib 1.0.1 then retry 
  pip install --force-reinstall -v "resolvelib==1.0.1"
  ansible-galaxy collection install --upgrade community.docker community.general
fi

ansible-playbook app-installs.yml -i hosts.yaml -e "login_user=$USER" -e "zerotier_network=e5cd7a9e1c66f72f"

